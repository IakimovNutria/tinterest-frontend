export const canChangeChat = (role: string | undefined) => {
	if (!role) {
		return false;
	}

	return role === 'Owner' || role === 'Moderator';
};

export const isChatOwner = (role: string | undefined) => {
	if (!role) {
		return false;
	}

	return role === 'Owner';
};