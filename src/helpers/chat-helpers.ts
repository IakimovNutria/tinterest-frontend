
export function getActiveFirstColor(isActive: boolean) {
	return isActive ? '#CFCFCF' : '#000';
}

export function getActiveSecondColor(isActive: boolean) {
	return isActive ? '#000' : '#CFCFCF';
}
