import { useCookies } from 'react-cookie';
import { useQuery } from '@tanstack/react-query';
import { getUserFn } from '../services/api';
import { Actions, useStateContext } from '../context';
import { Loader } from '../components/loader/loader';
import React from 'react';

type AuthMiddlewareProps = {
    children: React.ReactElement;
};

const AuthMiddleware: React.FC<AuthMiddlewareProps> = ({ children }) => {
	const [cookies] = useCookies(['access_token']);
	const stateContext = useStateContext();

	const query = useQuery(['authUser'], getUserFn, {
		enabled: !!cookies.access_token,
		select: (data) => data.data,
		onSuccess: (data) => {
			stateContext.dispatch({ type: Actions.SET_USER, payload: data });
		},
	});

	if (query.isLoading && cookies.access_token) {
		return <Loader />;
	}

	return children;
};

export default AuthMiddleware;
