import React, { useState, useCallback, useEffect, useMemo } from 'react';
import { Box, Grid, Typography } from '@mui/material';
import { useQuery } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';
import ChatItem from '../../components/chat-item/chat-item';
import Chat from '../../components/chat/chat';
import { useStateContext } from '../../context';
import { getChatPreviews } from '../../services/api';

const HomePage = () => {
	const navigate = useNavigate();
	const stateContext = useStateContext();
	const user = stateContext.state.authUser;

	useEffect(() => void(!user && navigate('/login')), [user]);
	const { data } = useQuery(['chatPreviews'], getChatPreviews, {
		refetchInterval: 500,
	});
	const chatPreviews = data?.data.chat_previews;
	const [isItemDelete, setIsItemDelete] = useState(false);
	const [activeChatId, setActiveChatId] = useState(chatPreviews ? chatPreviews[0]?.id : undefined);
	useEffect(() => {
		(activeChatId && isItemDelete) || setActiveChatId(chatPreviews ? chatPreviews[0]?.id : undefined);

		if (isItemDelete) {
			setIsItemDelete(false);
		}
	}, [chatPreviews, isItemDelete, setIsItemDelete]);
	const changeChatId = useCallback((newChatId: number) => setActiveChatId(newChatId), [setActiveChatId]);
	const isChatActive = useCallback((chatId: number) => activeChatId === chatId, [activeChatId]);
	const activeChat = useMemo(() => {
		return chatPreviews?.find((chat) => chat.id === activeChatId) ?? (chatPreviews ? chatPreviews[chatPreviews.length - 1] : undefined);
	}, [activeChatId, chatPreviews]);

	return chatPreviews?.length ? (
		<Grid
			container
			justifyContent="center"
			sx={{ height: '100%' }}
			flexWrap="nowrap"
		>
			<Grid
				item
				sx={{ width: '20%' }}
			>
				<Box
					sx={{
						backgroundColor: '#424242',
						height: '100%',
						borderRight: '5px solid #FFDD2D',
						position: 'relative',
					}}
				>
					{chatPreviews.map((chat) => (
						<ChatItem
							key={chat.id}
							chatPreview={chat}
							isActive={isChatActive(chat.id)}
							onClick={() => changeChatId(chat.id)}
						/>
					))}
				</Box>
			</Grid>
			{
				activeChat &&
				<Grid
					item
					sx={{
						width: '80%',
					}}
				>
					<Chat
						id={activeChat.id}
						lastMessageId={activeChat.last_message?.id}
						unreadMessagesCount={activeChat.unread_message_count}
						name={activeChat.name}
						onItemDelete={() => setIsItemDelete(true)}
					/>
				</Grid>
			}
		</Grid>
	) : (
		<Box sx={{
			width: '100%',
			height: '100%',
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center',
		}}>
			<Typography sx={{
				color: '#f9d13e',
				fontWeight: 600,
				fontSize: { xs: '2rem', md: '3rem' },
				mb: 2,
				letterSpacing: 1,
			}}>
				Здесь будут отображаться ваши чаты
			</Typography>
		</Box>
	);
};

export default HomePage;
