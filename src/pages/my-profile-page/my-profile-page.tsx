import React, { useCallback } from 'react';
import { Box, Typography } from '@mui/material';
import { useQuery } from '@tanstack/react-query';
import { AllTags, ChosenTags } from '../../components/tags-chose/tags-chose';
import { useStateContext } from '../../context';
import {
	bindTagToUser,
	getRecommendedChats, getRecommendedUsers,
	getUsersTags,
	unbindUserTag
} from '../../services/api';
import { IChosenTag } from '../../services/api.types';
import { RecChatPreview } from '../../components/rec-chat-preview/rec-chat-preview';
import { RecUserPreview } from '../../components/rec-user-preview/rec-user-preview';


function MyProfilePage(): JSX.Element {
	const stateContext = useStateContext();
	const { data: tagsData, refetch: tagsRefetch } = useQuery(
		['userTags'],
		() => getUsersTags(stateContext.state.authUser?.id ?? 0),
		{
			enabled: !!stateContext.state.authUser?.id,
		}
	);
	const { data: chatsData } = useQuery(
		['recommendedChats'],
		() => getRecommendedChats()
	);
	const { data: usersData, refetch: refetchUsers } = useQuery(
		['recommendedUsers'],
		() => getRecommendedUsers()
	);
	const onBindTag = useCallback((tag: IChosenTag, rating: number) => {
		bindTagToUser(tag.tag_id, rating).then(() => tagsRefetch());
	}, [tagsRefetch, bindTagToUser]);
	const onUnbindTag = useCallback((tagId: number) => {
		unbindUserTag(tagId).then(() => tagsRefetch());
	}, [tagsRefetch, unbindUserTag]);

	return (
		<Box sx={{
			display: 'flex',
			width: '100%',
			height: 'calc(100% - 8px)',
			paddingTop: '8px',
			backgroundColor: '#444040',
		}}>
			<Box sx={{
				width: '33%',
				textAlign: 'center',
				display: 'flex',
				flexDirection: 'row',
				padding: '10px',
			}}>
				<Typography
					sx={{
						borderRadius: '100%',
						backgroundColor: '#CFCFCF',
						width: '200px',
						height: '200px',
						textAlign: 'center',
						lineHeight: '200px',
						marginBlockEnd: '0',
						marginBlockStart: '0',
						fontSize: '49px',
						fontWeight: '800',
					}}
				>
					{stateContext.state.authUser?.username.slice(0, 2)}
				</Typography>

				<Box
					sx={{
						display: 'flex',
						flexDirection: 'column',
						margin: '10px',
						paddingLeft: '20px',
						paddingRight: '20px',
						backgroundColor: '#CFCFCF',
						height: '200px',
						width: 'calc(100% - 300px)',
						borderRadius: '10px',
					}}
				>
					<Typography sx={{
						fontSize: '16px',
						fontWeight: '800',
					}}>
						Username:
					</Typography>
					<Typography sx={{
						fontSize: '24px',
						fontWeight: '800',
					}}>
						{stateContext.state.authUser?.username}
					</Typography>
					<Typography sx={{
						fontSize: '16px',
						fontWeight: '800',
					}}>
						Email:
					</Typography>
					<Typography sx={{
						fontSize: '20px',
						fontWeight: '800',
					}}>
						{stateContext.state.authUser?.email}
					</Typography>
					<Typography sx={{
						fontSize: '16px',
						fontWeight: '800',
					}}>
						Telegram username:
					</Typography>
					<Typography sx={{
						fontSize: '20px',
						fontWeight: '800',
					}}>
						{stateContext.state.authUser?.telegram_username || '-'}
					</Typography>
				</Box>
			</Box>

			<Box sx={{
				width: '40%',
				display: 'flex',
				flexDirection: 'column',
				alignItems: 'center',
				borderRight: '2px solid #c9a10e',
				borderLeft: '2px solid #c9a10e',
			}}>
				<Typography sx={{
					fontSize: { xs: '1rem', md: '2rem' },
					mb: 2,
					fontWeight: 600,
					letterSpacing: 0.5,
					marginBottom: '8px',
					color: '#f9d13e',
				}}>Интересы</Typography>
				<Box sx={{
					display: 'flex',
					width: '100%',
				}}>
					<Box sx={{
						width: '50%',
					}}>
						<AllTags
							header="Откройте новые интересы"
							chosenTags={tagsData?.data.tag_user_bindings ?? []}
							onUnbindTag={onUnbindTag}
							onBindTag={onBindTag}
						/>
					</Box>
					<Box sx={{
						width: '50%',
					}}>
						<ChosenTags
							header="Ваши интересы"
							chosenTags={tagsData?.data.tag_user_bindings ?? []}
							onUnbindTag={onUnbindTag}
							isRating
							onUpdateTag={tagsRefetch}
						/>
					</Box>
				</Box>
			</Box>

			<Box sx={{
				width: '27%',
				display: 'flex',
				flexDirection: 'column',
				alignItems: 'center',
				overflow: 'auto',
			}}>
				<Typography sx={{
					fontSize: { xs: '0.5rem', md: '1.5rem' },
					mb: 2,
					fontWeight: 600,
					letterSpacing: 0.5,
					marginBottom: '8px',
					color: '#f9d13e',
					textAlign: 'center',
				}}>
					Рекомендованные чаты
				</Typography>
				<ul style={{
					listStyle: 'none',
					padding: 0,
					margin: 0,
					display: 'flex',
					flexDirection: 'column',
					gap: '8px',
					width: '80%',
				}}>
					{chatsData?.data.chats.map((chat) => (
						<li key={chat.id}>
							<RecChatPreview {...chat} />
						</li>
					))}
				</ul>
				<Typography sx={{
					fontSize: { xs: '0.5rem', md: '1.5rem' },
					mb: 2,
					fontWeight: 600,
					letterSpacing: 0.5,
					marginBottom: '8px',
					color: '#f9d13e',
					textAlign: 'center',
				}}>
					Рекомендованные пользователи
				</Typography>
				<ul style={{
					listStyle: 'none',
					padding: 0,
					margin: 0,
					display: 'flex',
					flexDirection: 'column',
					gap: '8px',
					width: '80%',
				}}>
					{usersData?.data.users.map((user) => (
						<li key={user.id}>
							<RecUserPreview
								{...user}
								onCreatePersonalChat={refetchUsers}
							/>
						</li>
					))}
				</ul>
			</Box>
		</Box>
	);
}

export default MyProfilePage;