import React, { useEffect } from 'react';
import { Box, Container, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import { FormProvider, SubmitHandler, useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import FormInput from '../../components/form-input/form-input';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { LoadingButton as _LoadingButton } from '@mui/lab';
import { toast } from 'react-toastify';
import { useMutation, useQuery } from '@tanstack/react-query';
import { getUserFn, signUpUserFn } from '../../services/api';
import { Actions, useStateContext } from '../../context';
import { RegisterInput, registerSchema } from '../../constants/register';

const LoadingButton = styled(_LoadingButton)`
  padding: 0.6rem 0;
  background-color: #f9d13e;
  color: black;
  font-weight: 500;

  &:hover {
    background-color: #ebc22c;
    transform: translateY(-2px);
  }
`;

const LinkItem = styled(Link)`
  text-decoration: none;
  color: black;
  &:hover {
    text-decoration: underline;
  }
`;

const Register = () => {
	const navigate = useNavigate();
	const location = useLocation();

	const from = ((location.state as any)?.from.pathname as string) || '/';

	const methods = useForm<RegisterInput>({
		resolver: zodResolver(registerSchema),
	});

	const stateContext = useStateContext();

	// API Get Current Logged-in user
	const query = useQuery(['authUser'], getUserFn, {
		enabled: false,
		select: (data) => data.data,
		retry: 1,
		onSuccess: (data) => {
			stateContext.dispatch({ type: Actions.SET_USER, payload: data });
			navigate(from);
		},
	});

	// API Login Mutation
	const { mutate: registerUser, isLoading } = useMutation(
		(userData: RegisterInput) => signUpUserFn(userData),
		{
			onSuccess: () => {
				query.refetch();
				toast.success('You successfully logged in');
			},
			// onError: (error: any) => {
			// 	if (Array.isArray((error as any).response.data.error)) {
			// 		(error as any).response.data.error.forEach((el: any) =>
			// 			toast.error(el.message, {
			// 				position: 'top-right',
			// 			})
			// 		);
			// 	} else {
			// 		toast.error((error as any).response.data.message, {
			// 			position: 'top-right',
			// 		});
			// 	}
			// },
		}
	);

	const {
		reset,
		handleSubmit,
		formState: { isSubmitSuccessful },
	} = methods;

	useEffect(() => {
		if (isSubmitSuccessful) {
			reset();
		}
	}, [isSubmitSuccessful]);

	const onSubmitHandler: SubmitHandler<RegisterInput> = (values) => {
		registerUser(values);
	};

	return (
		<Container
			maxWidth={false}
			sx={{
				display: 'flex',
				justifyContent: 'center',
				alignItems: 'center',
				minHeight: '100vh',
				backgroundColor: '#3C3838',
			}}
		>
			<Box
				sx={{
					display: 'flex',
					justifyContent: 'center',
					alignItems: 'center',
					flexDirection: 'column',
				}}
			>
				<Typography
					textAlign='center'
					component='h1'
					sx={{
						color: '#f9d13e',
						fontWeight: 600,
						fontSize: { xs: '2rem', md: '3rem' },
						mb: 2,
						letterSpacing: 1,
					}}
				>
					Регистрация
				</Typography>

				<FormProvider {...methods}>
					<Box
						component='form'
						onSubmit={handleSubmit(onSubmitHandler)}
						noValidate
						autoComplete='off'
						maxWidth='27rem'
						width='100%'
						sx={{
							backgroundColor: '#e5e7eb',
							p: { xs: '1rem', sm: '2rem' },
							borderRadius: 2,
						}}
					>
						<FormInput
							name='username'
							label='Username'
							type='username'
						/>
						<FormInput
							name='password'
							label='Password'
							type='password'
						/>
						<FormInput
							name='email'
							label='Email'
							type='email'
						/>
						<FormInput
							name='telegram_username'
							label='Telegram username'
							type='telegram_username'
						/>

						<LoadingButton
							variant='contained'
							sx={{ mt: 1 }}
							fullWidth
							disableElevation
							type='submit'
							loading={isLoading}
						>
							Зарегистрироваться
						</LoadingButton>

						<Typography sx={{ fontSize: '0.9rem', mt: '1rem' }}>
							<LinkItem to='/login'>Уже есть аккаунт?</LinkItem>
						</Typography>
					</Box>
				</FormProvider>
			</Box>
		</Container>
	);
};

export default Register;
