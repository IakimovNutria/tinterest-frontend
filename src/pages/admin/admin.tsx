import React, { useCallback } from 'react';
import { useQuery } from '@tanstack/react-query';
import { Box, Typography } from '@mui/material';
import { approveTag, deleteTag, getUnapprovedTags } from '../../services/api';

import './admin.css';

export const Admin: React.FC = () => {
	const { data: tagsPromise, refetch } = useQuery(
		['unapprovedTags'],
		() => getUnapprovedTags()
	);
	const onApprove = useCallback((tagId: number) => {
		approveTag(tagId).then(() => refetch());
	}, []);
	const onDelete = useCallback((tagId: number) => {
		deleteTag(tagId).then(() => refetch());
	}, []);

	return (
		<ul style={{
			listStyle: 'none',
			padding: '16px',
			margin: 0,
			display: 'flex',
			flexDirection: 'column',
			flexWrap: 'wrap',
			width: '100%',
			height: '100%',
			boxSizing: 'border-box',
		}}>
			{tagsPromise?.data.tags.map((tag) => (
				<li
					key={tag.id}
					style={{
						backgroundColor: '#CFCFCF',
						width: '320px',
						height: '40px',
						margin: '0 0 8px 0',
						borderRadius: '12px',
						display: 'flex',
						justifyContent: 'space-between',
						padding: '0 0 0 8px',
						overflow: 'hidden',
					}}
				>
					<Typography sx={{
						margin: 'auto 0',
					}}>
						{tag.name}
					</Typography>
					<Box sx={{
						display: 'flex',
					}}>
						<button
							className="approve-button"
							onClick={() => onApprove(tag.id)}
						>
							<Typography sx={{
								fontSize: '12px',
							}}>
                                Принять
							</Typography>
						</button>
						<button
							className="not-approve-button"
							onClick={() => onDelete(tag.id)}
						>
							<Typography sx={{
								fontSize: '12px',
							}}>
                                Удалить
							</Typography>
						</button>
					</Box>
				</li>
			))}
		</ul>
	);
};
