import React from 'react';
import { Box, Typography } from '@mui/material';
import { useQuery } from '@tanstack/react-query';
import { useNavigate, useParams } from 'react-router-dom';
import { useStateContext } from '../../context';
import { getUserById, getUsersTags } from '../../services/api';
import { ChosenTags } from '../../components/tags-chose/tags-chose';

function UserProfilePage(): JSX.Element {
	const { id } = useParams();
	const parsedId = parseInt(id || '0', 10);
	const navigate = useNavigate();
	const context = useStateContext();
	if (parsedId === context.state.authUser?.id) {
		navigate('/profile');
	}

	const { data: userData } = useQuery(
		['userInfo'],
		() => getUserById(parsedId)
	);
	const { data: tagsData } = useQuery(
		['userTags'],
		() => getUsersTags(parsedId)
	);

	return (
		userData ?
			<Box sx={{
				display: 'flex',
				width: '100%',
				height: 'calc(100% - 8px)',
				paddingTop: '8px',
				backgroundColor: '#444040',
			}}>
				<Box sx={{
					width: '50%',
					textAlign: 'center',
					display: 'flex',
					flexDirection: 'row',
					padding: '10px',
				}}>
					<Typography
						sx={{
							borderRadius: '100%',
							backgroundColor: '#CFCFCF',
							width: '200px',
							height: '200px',
							textAlign: 'center',
							lineHeight: '200px',
							marginBlockEnd: '0',
							marginBlockStart: '0',
							fontSize: '49px',
							fontWeight: '800',
						}}
					>
						{userData?.data.username.slice(0, 2)}
					</Typography>

					<Box
						sx={{
							display: 'flex',
							flexDirection: 'column',
							margin: '10px',
							paddingLeft: '20px',
							paddingRight: '20px',
							backgroundColor: '#CFCFCF',
							height: '200px',
							width: 'calc(100% - 300px)',
							borderRadius: '10px',
						}}
					>
						<Typography sx={{
							fontSize: '16px',
							fontWeight: '800',
						}}>
							Username:
						</Typography>
						<Typography sx={{
							fontSize: '24px',
							fontWeight: '800',
						}}>
							{userData?.data.username}
						</Typography>
						<Typography sx={{
							fontSize: '16px',
							fontWeight: '800',
						}}>
							Email:
						</Typography>
						<Typography sx={{
							fontSize: '20px',
							fontWeight: '800',
						}}>
							{userData?.data.email}
						</Typography>
						<Typography sx={{
							fontSize: '16px',
							fontWeight: '800',
						}}>
							Telegram username:
						</Typography>
						<Typography sx={{
							fontSize: '20px',
							fontWeight: '800',
						}}>
							{userData?.data.telegram_username || '-'}
						</Typography>
					</Box>
				</Box>
				<Box sx={{
					width: '50%',
					display: 'flex',
					flexDirection: 'column',
					alignItems: 'center',
					borderRight: '2px solid #c9a10e',
					borderLeft: '2px solid #c9a10e',
				}}>
					<Box sx={{
						width: '50%',
					}}>
						<ChosenTags
							header="Интересы"
							chosenTags={tagsData?.data.tag_user_bindings ?? []}
							isRatingReading
						/>
					</Box>
				</Box>
			</Box>
			:
			<Box sx={{
				height: '100%',
				width: '100%',
				alignItems: 'center',
				display: 'flex',
			}}>
				<Typography sx={{
					color: '#CFCFCF',
					fontSize: '100px',
				}}>
					No such user
				</Typography>
			</Box>
	);
}

export default UserProfilePage;