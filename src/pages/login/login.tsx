import React, { useEffect } from 'react';
import { Box, Container, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import { FormProvider, SubmitHandler, useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import FormInput from '../../components/form-input/form-input';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { LoadingButton as _LoadingButton } from '@mui/lab';
import { toast } from 'react-toastify';
import { useMutation, useQuery } from '@tanstack/react-query';
import { getUserFn, loginUserFn } from '../../services/api';
import { Actions, useStateContext } from '../../context';
import { LoginInput, loginSchema } from '../../constants/login';

const LoadingButton = styled(_LoadingButton)`
  padding: 0.6rem 0;
  background-color: #f9d13e;
  color: #000;
  font-weight: 500;

  &:hover {
    background-color: #ebc22c;
    transform: translateY(-2px);
  }
`;

const LinkItem = styled(Link)`
  text-decoration: none;
  color: #000;
  &:hover {
    text-decoration: underline;
  }
`;

const LoginPage = () => {
	const navigate = useNavigate();
	const location = useLocation();

	const from = ((location.state as any)?.from.pathname as string) || '/';

	const methods = useForm<LoginInput>({
		resolver: zodResolver(loginSchema),
	});

	const stateContext = useStateContext();

	const query = useQuery(['authUser'], getUserFn, {
		retry: 1,
		select: (data) => data.data,
		onSuccess: (data) => {
			stateContext.dispatch({ type: Actions.SET_USER, payload: data });
			navigate(from);
		},
	});

	// API Login Mutation
	const { mutate: loginUser, isLoading } = useMutation(
		(userData: LoginInput) => loginUserFn(userData),
		{
			onSuccess: () => {
				query.refetch();
				toast.success('You successfully logged in');
			},
			// onError: (error: any) => {
			// 	if (Array.isArray((error as any).response.data.error)) {
			// 		(error as any).response.data.error.forEach((el: any) =>
			// 			toast.error(el.message, {
			// 				position: 'top-right',
			// 			})
			// 		);
			// 	} else {
			// 		toast.error((error as any).response.data.message, {
			// 			position: 'top-right',
			// 		});
			// 	}
			// },
		}
	);

	const {
		reset,
		handleSubmit,
		formState: { isSubmitSuccessful },
	} = methods;

	useEffect(() => {
		if (isSubmitSuccessful) {
			reset();
		}
	}, [isSubmitSuccessful]);

	useEffect(() => void(stateContext.state.authUser && navigate('/')), [stateContext]);

	const onSubmitHandler: SubmitHandler<LoginInput> = (values) => {
		loginUser(values);
	};

	return (
		<Container
			maxWidth={false}
			sx={{
				display: 'flex',
				justifyContent: 'center',
				alignItems: 'center',
				minHeight: '100vh',
				backgroundColor: '#3C3838',
			}}
		>
			<Box
				sx={{
					display: 'flex',
					justifyContent: 'center',
					alignItems: 'center',
					flexDirection: 'column',
				}}
			>
				<Typography
					textAlign='center'
					component='h1'
					sx={{
						color: '#f9d13e',
						fontWeight: 600,
						fontSize: { xs: '2rem', md: '3rem' },
						mb: 2,
						letterSpacing: 1,
					}}
				>
					С возвращением!
				</Typography>

				<FormProvider {...methods}>
					<Box
						component='form'
						onSubmit={handleSubmit(onSubmitHandler)}
						noValidate
						autoComplete='off'
						maxWidth='27rem'
						width='100%'
						sx={{
							backgroundColor: '#e5e7eb',
							p: { xs: '1rem', sm: '2rem' },
							borderRadius: 2,
						}}
					>
						<FormInput name='username'
							label='Username'
							type='username' />
						<FormInput name='password'
							label='Password'
							type='password' />

						<LoadingButton
							variant='contained'
							sx={{ mt: 1 }}
							fullWidth
							disableElevation
							type='submit'
							loading={isLoading}
						>
							Войти
						</LoadingButton>

						<Typography sx={{ fontSize: '0.9rem', mt: '1rem' }}>
							<LinkItem to='/register'>Нет аккаунта?</LinkItem>
						</Typography>
					</Box>
				</FormProvider>
			</Box>
		</Container>
	);
};

export default LoginPage;
