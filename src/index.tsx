import React from 'react';
import ReactDOM from 'react-dom/client';
import App from '../src/components/app/app';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { StateContextProvider } from './context';
import { BrowserRouter as Router } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const queryClient = new QueryClient({
	defaultOptions: {
		queries: {
			staleTime: 5 * 1000,
		},
	},
});

const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
);

root.render(
	<React.StrictMode>
		<QueryClientProvider client={queryClient}>
			<Router>
				<StateContextProvider>
					<div>
						<ToastContainer />
						<App />
					</div>
				</StateContextProvider>
				{
					//<ReactQueryDevtools initialIsOpen={false} />
				}
			</Router>
		</QueryClientProvider>
	</React.StrictMode>
);
