export enum ChatType {
    PERSONAL = 'Personal',
    PUBLIC = 'Public',
    PRIVATE = 'Private',
    SECRET = 'Secret',
}
