import { object, string, TypeOf } from 'zod';

export const loginSchema = object({
	username: string()
		.min(1, 'username is required'),
	password: string()
		.min(1, 'Password is required')
		.min(8, 'Password must be more than 8 characters'),
});

export type LoginInput = TypeOf<typeof loginSchema>;
