import { object, string, TypeOf } from 'zod';

export const registerSchema = object({
	username: string()
		.min(1, 'username is required'),
	password: string()
		.min(1, 'Password is required')
		.min(8, 'Password must be more than 8 characters'),
	email: string()
		.min(1, 'Email address is required')
		.email('Email Address is invalid'),
	telegram_username: string(),
});

export type RegisterInput = TypeOf<typeof registerSchema>;
