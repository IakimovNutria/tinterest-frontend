import React, { useState, useEffect } from 'react';
import { useCookies } from 'react-cookie';
import { AppBar, Box, Container, Toolbar, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';
import { useQuery } from '@tanstack/react-query';
import { LoadingButton as _LoadingButton } from '@mui/lab';
import { Actions, useStateContext } from '../../context';
import { AddChatModal } from '../add-chat-modal/add-chat-modal';
import { getUnapprovedTags } from '../../services/api';
import { ITag } from '../../services/api.types';

const LoadingButton = styled(_LoadingButton)`
  padding: 0.4rem;
  color: #222;
  font-weight: 500;

  &:hover {
	background-color: #AFAFAF;
    transform: translateY(-2px);
  }
`;

const Header = () => {
	const navigate = useNavigate();
	const [, , removeToken] = useCookies(['access_token']);
	const stateContext = useStateContext();
	const { data: tagsPromise } = useQuery(
		['unapprovedTags'],
		() => getUnapprovedTags()
	);
	const [tags, setTags] = useState<ITag[] | undefined>(undefined);
	useEffect(() => {
		setTags(tagsPromise?.status === 403 ? undefined : tagsPromise?.data.tags);
	}, [tagsPromise, setTags]);
	const [isAddChatModalActive, setIsAddChatModalActive] = useState(false);
	const user = stateContext.state.authUser;

	const onLogoutHandler = async () => {
		removeToken('access_token');
		stateContext.dispatch({ type: Actions.SET_USER, payload: null });
		window.location.href = '/login';
	};

	return (
		<>
			{
				isAddChatModalActive &&
				<AddChatModal
					onClose={() => setIsAddChatModalActive(false)}
					isActive={isAddChatModalActive}
				/>
			}
			<AppBar position='static'
				sx={{ backgroundColor: '#CFCFCF' }}>
				<Container maxWidth='lg'>
					<Toolbar>
						<Typography
							variant='h6'
							onClick={() => navigate('/profile')}
							sx={{
								borderRadius: '10px',
								padding: '0.4rem 1rem 0.4rem 1rem',
								cursor: 'pointer',
								color: '#222',
								':hover': {
									backgroundColor: '#AFAFAF',
									transform: 'translateY(-2px)',
								},
							}}
						>
                            Tinterest
						</Typography>
						<Box display='flex'
							sx={{ ml: 'auto' }}
						>
							{tags &&
								<LoadingButton onClick={() => navigate('/admin')}>
									Админ
								</LoadingButton>
							}
							<LoadingButton
								onClick={() => navigate('/')}
							>
								Чаты
							</LoadingButton>
							<LoadingButton
								onClick={() => setIsAddChatModalActive(true)}
							>
								Добавить чат
							</LoadingButton>
							<LoadingButton
								onClick={() => navigate('/profile')}
							>
								Профиль
							</LoadingButton>
							<LoadingButton
								onClick={onLogoutHandler}
							>
								Выйти
							</LoadingButton>
						</Box>
					</Toolbar>
				</Container>
			</AppBar>
		</>
	);
};

export default Header;
