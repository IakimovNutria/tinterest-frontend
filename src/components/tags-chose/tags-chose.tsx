import React, { useCallback, useState } from 'react';
import { Box, Button as Button2, Typography } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import ClearIcon from '@mui/icons-material/Clear';
import { useQuery } from '@tanstack/react-query';
import {
	AllTagsProps, ChosenTagsProps,
	CreateTagButtonProps,
	CreateTagFormProps,
	TagListProps
} from './tags-chose.types';
import { createTag, getTags, updateUserTag } from '../../services/api';
import { Accordion, AccordionSummary, AccordionDetails } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

import { Input } from '../input/input';
import { Modal } from '../modal/modal';
import Button from '../button/button';

export const AllTags = (props: AllTagsProps) => {
	const { header, headerColor } = props;

	return (
		<Box sx={{
			display: 'flex',
			flexDirection: 'column',
			alignItems: 'center',
		}}>
			<Box sx={{
				marginBottom: '16px',
			}}>
				<Typography sx={{
					fontSize: { xs: '0.7rem', md: '1.4rem' },
					mb: 2,
					fontWeight: 600,
					letterSpacing: 0.5,
					marginBottom: '8px',
					color: headerColor ? headerColor : '#f9d13e',
					textAlign: 'center',
				}}>
					{header}
				</Typography>
			</Box>
			<Box sx={{
				display: 'flex',
			}}>
				<Box sx={{
					marginRight: '8px',
				}}>

				</Box>
				<Box sx={{
					display: 'flex',
					flexDirection: 'column-reverse',
				}}>
					<TagList
						chosenTags={props.chosenTags}
						onBindTag={props.onBindTag}
						onUnbindTag={props.onUnbindTag}
					/>
				</Box>
			</Box>
		</Box>
	);
};

export const ChosenTags = (props: ChosenTagsProps) => {
	const onUnbind = props.onUnbindTag || function() { return; };
	const onChangeRating = useCallback((tagId: number, rating: number) => {
		updateUserTag(tagId, rating).then(() => {
			props.onUpdateTag && props.onUpdateTag();
		});
	}, [props.onUpdateTag]);
	return (
		<Box sx={{
			display: 'flex',
			flexDirection: 'column',
			alignItems: 'center',
		}}>
			<Typography sx={{
				fontSize: { xs: '0.7rem', md: '1.4rem' },
				mb: 2,
				fontWeight: 600,
				letterSpacing: 0.5,
				marginBottom: '8px',
				color: props.headerColor ? props.headerColor : '#f9d13e',
			}}>
				{props.header}
			</Typography>
			{
				Boolean(props.chosenTags.length) &&
				(props.isRatingReading || props.isRating) &&
				<Box sx={{
					width: '70%',
					padding: '0 8px',
					boxSizing: 'border-box',
					height: '40px',
					borderRadius: '8px',
					display: 'flex',
					justifyContent: 'space-between',
					alignItems: 'center',
					overflow: 'hidden',
				}}>
					<Typography sx={{
						color: '#f9d13e',
					}}>
						Имя
					</Typography>
					<Typography sx={{
						color: '#f9d13e',
					}}>
						Рейтинг
					</Typography>
				</Box>
			}
			<ul style={{
				padding: 0,
				listStyle: 'none',
				margin: 0,
				display: 'flex',
				flexDirection: 'column',
				alignItems: 'end',
				gap: '8px',
				width: '70%',
			}}>
				{props.chosenTags.sort((a, b) => a.tag_name.localeCompare(b.tag_name)).map((tag) => (
					<li
						key={tag.tag_id}
						style={{
							width: '100%',
							height: '40px',
							backgroundColor: '#CFCFCF',
							borderRadius: '8px',
							boxSizing: 'border-box',
							display: 'flex',
							padding: '0 8px',
							justifyContent: 'space-between',
							alignItems: 'center',
							overflow: 'hidden',
						}}
					>
						<Typography sx={{
							maxWidth: '128px',
							overflow: 'hidden',
							textOverflow: 'ellipsis',
						}}>
							{tag.tag_name}
						</Typography>
						<Box sx={{
							display: 'flex',
							justifyContent: 'center',
						}}>
							{
								props.isRating &&
								<div style={{
									display: 'flex',
									justifyContent: 'center',
								}}>
									<input
										type="number"
										min={1}
										max={10}
										value={tag.rating}
										onChange={(e) => !isNaN(Number(e.target.value)) && Number(e.target.value) >= 1 && Number(e.target.value) <= 10 && onChangeRating(tag.tag_id, Number(e.target.value))}
									/>
								</div>
							}
							{
								props.isRatingReading &&
								<div style={{
									display: 'flex',
									justifyContent: 'center',
								}}>
									<Typography>
										{tag.rating}
									</Typography>
								</div>
							}
							{
								props.onUnbindTag !== undefined &&
								<ClearIcon
									onClick={() => onUnbind(tag.tag_id)}
									sx={{
										cursor: 'pointer',
										borderRadius: '8px',
										':hover': {
											backgroundColor: '#AFAFAF',
										},
									}}
								/>
							}
						</Box>
					</li>
				))}
			</ul>
		</Box>
	);
};


export const CreateTagButton = (props: CreateTagButtonProps) => {
	const { parentTagId, refetchFunction } = props;
	const [isAddChatModalActive, setIsAddChatModalActive] = useState(false);

	return <>
		{
			isAddChatModalActive &&
			<CreateTagForm
				onClose={() => {
					setIsAddChatModalActive(false);
					refetchFunction();
				}}
				isActive={isAddChatModalActive}
				parentTagId={parentTagId}
			/>
		}
		<Button2
			onClick={() => setIsAddChatModalActive(true)}
			sx={{
				marginLeft: '8px',
				backgroundColor: '#AFAFAF',
				':hover': {
					backgroundColor: '#8F8F8F',
				},
			}}
		>
			создать
		</Button2>
	</>;
};


export const CreateTagForm = (props: CreateTagFormProps) => {
	const { onClose, isActive, parentTagId } = props;

	const [tagName, setTagName] = useState('');
	const [tagDescription, setTagDescription] = useState('');
	const createTagButton = useCallback(async () => {
		await createTag(tagName, tagDescription, parentTagId);
		onClose();
	}, [tagName, tagDescription, parentTagId]);
	return (
		<Modal
			onClose={onClose}
			isActive={isActive}
		>
			<Box sx={{
				width: '500px',
				height: '400px',
				display: 'flex',
			}}>
				<Box sx={{
					height: '100%',
					backgroundColor: 'white',
					borderRadius: '16px 0 0 16px',
					overflow: 'hidden',
				}}>

				</Box>
				<Box sx={{
					height: '100%',
					width: '400px',
					display: 'flex',
					backgroundColor: '#424242',
					borderRadius: '0 16px 16px 0',
					overflow: 'hidden',
					justifyContent: 'space-between',
					flexDirection: 'column',
					alignItems: 'center',
				}}>
					<Box sx={{
						marginTop: '24px',
						marginBottom: '16px',
					}}>
						<Typography sx={{
							marginBottom: '8px',
							fontSize: { xs: '0.5rem', md: '1rem' },
							mb: 2,
							letterSpacing: 0.5,
						}}>
                            Название тега:
						</Typography>
						<Input
							placeholder="Tag name"
							onChange={(e) => setTagName(e.target.value)}
							value={tagName}
						/>
					</Box>
					<Box sx={{
						marginBottom: '16px',
					}}>
						<Typography sx={{
							marginBottom: '8px',
							fontSize: { xs: '0.5rem', md: '1rem' },
							mb: 2,
							letterSpacing: 0.5,
						}}>
                            Описание Тега:
						</Typography>
						<Input
							placeholder="Tag descr"
							onChange={(e) => setTagDescription(e.target.value)}
							value={tagDescription}
						/>
					</Box>
					<Box sx={{
						display: 'flex',
						flexDirection: 'row-reverse',
						width: '100%',
						marginBottom: '16px',
					}}>
						<Button
							roundBorders="left"
							onClick={createTagButton}
						>
                            Создать
						</Button>
					</Box>
				</Box>
			</Box>
		</Modal>
	);
};


export const TagList = (props: TagListProps) => {
	const { parentTagId, children } = props;
	const { data, isLoading, refetch } = useQuery(['tags' + parentTagId], () => getTags(parentTagId));
	const tags = isLoading ? undefined : data?.data;

	return tags ? (
		<Box>
			{parentTagId ?
				<Accordion
					defaultExpanded={false}
					sx={{
						border: 'none',
						boxShadow: 0,
						minHeight: '32px',
						borderRadius: '8px',
						backgroundColor: '#CFCFCF',
						'&.Mui-expanded': { minHeight: '32px' },
					}}
				>
					<AccordionSummary
						sx={{
							border: 'none',
							minHeight: '32px',
							height: '32px',
							margin: '0 !important',
							'&.Mui-expanded': {
								margin: 0,
								minHeight: '32px',
							},
						}}
						expandIcon={
							<ExpandMoreIcon
								sx={{
									backgroundColor: '#CFCFCF',
									borderRadius: '8px',
									':hover': {
										backgroundColor: '#AFAFAF',
									},
								}}
							/>
						}
					>
						{children}
					</AccordionSummary>
					<AccordionDetails sx={{
						border: 'none',
					}}>
						{tags.tags.map((tag) => (
							<TagList
								key={tag.id}
								parentTagId={tag.id}
								chosenTags={props.chosenTags}
								onUnbindTag={props.onUnbindTag}
								onBindTag={props.onBindTag}
							>
								<Box sx={{
									display: 'flex',
									gap: '8px',
								}}>
									<Typography sx={{
										title: tag.description,
									}}>
										{tag.name}
									</Typography>
									{
										props.chosenTags
											.some((chosenTag) => chosenTag.tag_id === tag.id) ?
											<ClearIcon
												onClick={(e) => {
													e.stopPropagation();
													props.onUnbindTag(tag.id);
												}}
												sx={{
													borderRadius: '8px',
													':hover': {
														backgroundColor: '#AFAFAF',
													},
												}}
											/> :
											<AddIcon
												onClick={(e) => {
													e.stopPropagation();
													props.onBindTag(props.chosenTags
														.find((chosenTag) => chosenTag.tag_id === tag.id) ??
														{ tag_id: tag.id, tag_name: tag.name, id: -1, rating: 10 }, 10);
												}}
												sx={{
													borderRadius: '8px',
													':hover': {
														backgroundColor: '#AFAFAF',
													},
												}}
											/>
									}
								</Box>
							</TagList>
						))}
						<CreateTagButton
							refetchFunction={refetch}
							parentTagId={parentTagId}
							functionName={(x) => console.log(x)}
						/>
					</AccordionDetails>
				</Accordion> :
				<Box sx={{
				}}
				>
					{tags.tags.map((tag) => (
						<TagList
							key={tag.id}
							parentTagId={tag.id}
							chosenTags={props.chosenTags}
							onBindTag={props.onBindTag}
							onUnbindTag={props.onUnbindTag}
						>
							<Box sx={{
								display: 'flex',
								gap: '8px',
							}}>
								<Typography sx={{
									title: tag.name,
								}}>
									{tag.name}
								</Typography>
								{
									props.chosenTags
										.some((chosenTag) => chosenTag.tag_id === tag.id) ?
										<ClearIcon
											onClick={(e) => {
												e.stopPropagation();
												props.onUnbindTag(tag.id);
											}}
											sx={{
												borderRadius: '8px',
												':hover': {
													backgroundColor: '#AFAFAF',
												},
											}}
										/> :
										<AddIcon
											onClick={(e) => {
												e.stopPropagation();
												props.onBindTag(props.chosenTags
													.find((chosenTag) => chosenTag.tag_id === tag.id) ??
													{ tag_id: tag.id, tag_name: tag.name, id: -1, rating: 10 }, 10);
											}}
											sx={{
												borderRadius: '8px',
												':hover': {
													backgroundColor: '#AFAFAF',
												},
											}}
										/>
								}
							</Box>
						</TagList>
					))}
					<Box>
						<CreateTagButton
							refetchFunction={refetch}
							parentTagId={parentTagId}
							functionName={(x) => console.log(x)}
						/>
					</Box>
				</Box>
			}
		</Box>
	) : null;
};
