import { ModalProps } from '../modal/modal.types';
import { IChosenTag } from '../../services/api.types';

export interface ChosenTagsProps {
    headerColor?: string;
    header: string;
    chosenTags: IChosenTag[];
    onUnbindTag?: (tagId: number) => void;
    isRating?: boolean;
    onUpdateTag?: () => void;
    isRatingReading?: boolean;
}

export interface AllTagsProps {
    header: string;
    chosenTags: IChosenTag[];
    onUnbindTag: (tagId: number) => void;
    onBindTag: (tag: IChosenTag, rating: number) => void;
    headerColor?: string;
}

export interface TagListProps {
    chosenTags: IChosenTag[];
    parentTagId?: number;
    children?: JSX.Element;
    onUnbindTag: (tagId: number) => void;
    onBindTag: (tag: IChosenTag, rating: number) => void;
}

export interface CreateTagButtonProps {
    functionName: (value: string) => void,
    parentTagId?: number

    refetchFunction: () => void,
}

export interface CreateTagFormProps extends Omit<ModalProps, 'children'> {
    parentTagId?: number
}