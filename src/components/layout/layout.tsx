import React from 'react';
import { Outlet } from 'react-router-dom';
import Header from '../header/header';
import { Box } from '@mui/material';

const Layout = () => {
	return (
		<Box
			sx={{ width: '100vw', height: '100vh' }}
		>
			<Header />
			<Box sx={{
				height: 'calc(100% - 64px)',
			}}>
				<Outlet />
			</Box>
		</Box>
	);
};

export default Layout;
