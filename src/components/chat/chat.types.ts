export type ChatProps = {
    id: number;
    name: string;
    lastMessageId?: number;
    unreadMessagesCount: number;
    onItemDelete: () => void;
}