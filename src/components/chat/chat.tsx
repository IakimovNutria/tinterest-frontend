import React, { useState, useCallback, useEffect } from 'react';
import { useQuery } from '@tanstack/react-query';
import { Grid, Input, Box, Typography } from '@mui/material';
import SendIcon from '@mui/icons-material/Send';
import { ChatProps } from './chat.types';
import { getChatMessages, markMessageRead } from '../../services/api';
import { Message } from '../message/message';
import { sendMessage as serverSendMessage } from '../../services/api';
import { useStateContext } from '../../context';
import { ChatIcon } from '../chat-icon/chat-icon';
import { ChatInfoModal } from '../chat-info-modal/chat-info-modal';

function Chat(props: ChatProps): JSX.Element {
	const context = useStateContext();
	const [message, setMessage] = useState('');
	const [sendByEnter, setSendByEnter] = useState(false);
	const [isChatInfoModalActive, setIsChatInfoModalActive] = useState(false);
	const { data, isError, refetch: refetchMessages } = useQuery([`${props.id}-messages`], getChatMessages(props.id, props.lastMessageId ?? -1), {
		refetchInterval: 500,
	});
	useEffect(() =>
		void(data?.data.messages.length && markMessageRead(props.id, data?.data.messages[data?.data.messages.length - 1].id))
	, [data?.data.messages]);
	useEffect(() =>
		void(data?.data.messages.length && markMessageRead(props.id, data?.data.messages[data?.data.messages.length - 1].id))
	, []);

	const sendMessage = useCallback(async () => {
		if (message) {
			serverSendMessage(props.id, message).then(() => {
				refetchMessages();
			});
			setMessage('');
		}
	}, [setMessage, message, props.id, refetchMessages]);
	const inputOnChange = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
		e.preventDefault();
		if (!sendByEnter) {
			setMessage(e.target.value);
		}
		setSendByEnter(false);
	}, [setMessage, sendByEnter, setSendByEnter]);
	const handleKeyDown = useCallback(async (event: React.KeyboardEvent<HTMLInputElement>) => {
		if (event.key === 'Enter' && !event.shiftKey) {
			event.preventDefault();
			await sendMessage();
			setSendByEnter(true);
		}
	}, [sendMessage, setSendByEnter]);

	return (
		<>
			{
				isChatInfoModalActive &&
				<ChatInfoModal
					onClose={() => setIsChatInfoModalActive(false)}
					isActive={isChatInfoModalActive}
					name={props.name}
					id={props.id}
					onItemDelete={props.onItemDelete}
				/>
			}
			<Box
				sx={{
					background: 'url(img/chat-image.png)',
					height: '100%',
					display: 'flex',
					flexDirection: 'column',
				}}
			>
				<div
					style={{
						height: '64px',
						background: '#424242',
						flexShrink: 0,
						display: 'flex',
						cursor: 'pointer',
					}}
					onClick={() => setIsChatInfoModalActive(true)}
				>
					<Box sx={{
						margin: 'auto 0',
					}}>
						<ChatIcon
							name={props.name}
							backgroundColor="#CFCFCF"
							color="#000"
							iconStyle={{
								cursor: 'pointer',
							}}
						/>
					</Box>
					<Typography sx={{
						margin: 'auto 0',
						color: '#CFCFCF',
					}}>
						{props.name}
					</Typography>
				</div>
				<Box sx={{
					overflow: 'auto',
					display: 'flex',
					flexGrow: 1,
					flexDirection: 'column-reverse',
					paddingBottom: '4px',
				}}>
					<Box sx={{
						display: 'flex',
						flexDirection: 'column',
						gap: '4px',
					}}>
						{
							props.lastMessageId && data &&
							data.data.messages.map((message) => (
								<Message
									key={message.id}
									id={message.id}
									text={message.text}
									createdAt={message.created_at}
									userId={message.user_id}
									isRead={message.is_read}
								/>
							))
						}
						{
							isError && <div>error</div>
						}
					</Box>
				</Box>
				<Box
					sx={{
						minHeight: '40px',
						background: '#424242',
						flexShrink: 0,
					}}
				>
					<Input
						sx={{
							width: '100%',
							height: '100%',
							color: '#CFCFCF',
							fontFamily: 'Roboto',
							fontSize: '16px',
							fontStyle: 'normal',
							fontWeight: '300',
							lineHeight: 'normal',
						}}
						autoFocus
						placeholder="Написать сообщение..."
						multiline
						maxRows={10}
						startAdornment={<div style={{ width: '5px' }} />}
						endAdornment={
							<>
								<Grid
									container
									alignContent="flex-end"
									sx={{ width: '24px', height: '100%' }}
								>
									<SendIcon
										sx={{
											color: '#FFDD2D',
											cursor: 'pointer',
											height: '40px',
										}}
										onClick={sendMessage}
									/>
								</Grid>
								<div
									style={{
										width: '5px',
									}}
								/>
							</>
						}
						value={message}
						onChange={inputOnChange}
						onKeyDown={handleKeyDown}
					/>
				</Box>
			</Box>
		</>
	);
}


export default Chat;