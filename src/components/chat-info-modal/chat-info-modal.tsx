import React, { useState, useEffect, useCallback } from 'react';
import { AxiosError } from 'axios';
import { Box, Typography } from '@mui/material';
import { useQuery } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';
import { Modal } from '../modal/modal';
import {
	addUserToChat, bindTagToChat, blockUser, getChatInfo,
	getChatMembers, getChatTags,
	getUserById,
	getUserByUsername,
	leaveChat, unbindChatTag, updateUserAccessRole
} from '../../services/api';
import { ChatIcon } from '../chat-icon/chat-icon';
import { IChosenTag, IUser } from '../../services/api.types';
import { Input } from '../input/input';
import Button from '../button/button';
import { AllTags, ChosenTags } from '../tags-chose/tags-chose';

import './chat-info-modal.css';
import { canChangeChat, isChatOwner } from '../../helpers/chat-roles';
import { useStateContext } from '../../context';

type ChatInfoModalProps = {
    onClose: () => void;
    isActive: boolean;
	id: number;
	name: string;
	onItemDelete: () => void;
};

export const ChatInfoModal: React.FC<ChatInfoModalProps> = (props: ChatInfoModalProps) => {
	const context = useStateContext();
	const navigate = useNavigate();
	const [newUser, setNewUser] = useState('');
	const [error, setError] = useState<AxiosError | undefined>();
	const [isInterestsOpen, setIsInterestsOpen] = useState(false);
	const { data: tagsData, refetch: tagsRefetch } = useQuery(
		[`${props.id}-chat-tags`],
		() => getChatTags(props.id),
		{
			enabled: !!props.id,
		}
	);
	const openUserProfilePage = useCallback((userId: number) => {
		navigate(`/user/${userId}`);
	}, [navigate]);
	const { data: chatInfo, refetch: refetchChatInfo } = useQuery([`${props.id}-chat-info`], () => getChatInfo(props.id));
	const { onClose, isActive, name, id } = props;
	const { data: chatMembersIds, refetch: refetchChatMembersIds } = useQuery([`${id}-chat-members`], getChatMembers(id));
	const [chatMembers, setChatMembers] = useState<(IUser & { membership_type: string } | undefined)[]>([]);
	const addUserToChatHandler = useCallback(() => {
		newUser && getUserByUsername(newUser).then((res) => {
			if (!chatMembersIds?.data.chat_members.some((chatMember) => chatMember.user_id === res.data.id)) {
				addUserToChat(props.id, res.data.id).then(() => { refetchChatInfo(); refetchChatMembersIds(); });
			}

			setError(undefined);
			setNewUser('');
		}).catch((err) => {
			setError(err);
			setNewUser('');
		});
	}, [props.id, newUser, setNewUser, refetchChatInfo, refetchChatMembersIds]);
	const leaveChatHandler = useCallback(() => {
		leaveChat(props.id).then(() => {
			props.onItemDelete();
			props.onClose();
		});
	}, [props.id, props.onItemDelete]);
	const onBindTag = useCallback((tag: IChosenTag) => {
		bindTagToChat(props.id, tag.tag_id).then(() => {
			tagsRefetch();
		});
	}, [tagsRefetch, props.id]);
	const onUnbindTag = useCallback((tagId: number) => {
		unbindChatTag(props.id, tagId).then(() => {
			tagsRefetch();
		});
	}, [tagsRefetch, props.id]);
	const onBlockUser = useCallback((userId: number) => {
		blockUser(props.id, userId).then(() => {
			refetchChatMembersIds();
		});
	}, [refetchChatMembersIds]);
	const onAddModerator = useCallback((userId: number) => {
		updateUserAccessRole(props.id, userId, 'Moderator').then(() => {
			refetchChatMembersIds();
		});
	}, [refetchChatMembersIds]);
	const onDeleteModerator = useCallback((userId: number) => {
		updateUserAccessRole(props.id, userId, 'Member').then(() => {
			refetchChatMembersIds();
		});
	}, [refetchChatMembersIds]);
	useEffect(() => {
		const members = chatMembersIds?.data.chat_members ?? [];
		const result = new Array(members.length) as ((IUser & { membership_type: string }) | undefined)[];

		for (let i = 0; i < members.length; i++) {
			getUserById(members[i].user_id).then((res) => {
				result[i] = { ...res.data, membership_type: members[i].membership_type };
			});
		}

		setChatMembers(result);
	}, [setChatMembers, chatMembersIds]);

	return (
		<Modal
			onClose={onClose}
			isActive={isActive}
		>
			<Box sx={{
				display: 'flex',
			}}>
				{!isInterestsOpen ?
					<Box sx={{
						backgroundColor: '#CFCFCF',
						width: '400px',
						height: '500px',
						padding: '16px',
						boxSizing: 'border-box',
						display: 'flex',
						alignItems: 'center',
						flexDirection: 'column',
						justifyContent: 'space-between',
					}}>
						<Box sx={{
							display: 'flex',
							alignItems: 'center',
							flexDirection: 'column',
						}}>
							<>
								<Typography sx={{
									fontSize: { xs: '1rem', md: '2rem' },
									mb: 2,
									letterSpacing: 1,
								}}>
									{props.name}
								</Typography>
								<Box sx={{
									display: 'flex',
									flexDirection: 'column',
								}}>
									{
										canChangeChat(chatInfo?.data.role) &&
										chatInfo?.data.type !== 'Personal' &&
										<Box sx={{
											display: 'flex',
											justifyContent: 'center',
										}}>
											<Box sx={{
												marginRight: '8px',
												display: 'flex',
												alignItems: 'center',
												flexDirection: 'column',
											}}>
												<>
													<Typography sx={{
														fontSize: { xs: '0.5rem', md: '1rem' },
														mb: 2,
														letterSpacing: 0.5,
														marginBottom: '8px',
													}}>
														Добавить участника
													</Typography>
													<Input
														value={newUser}
														onChange={(e) => setNewUser(e.target.value)}
														placeholder="username"
													/>
												</>
											</Box>
											<Box sx={{
												display: 'flex',
												flexDirection: 'column-reverse',
											}}>
												<Button
													roundBorders="all"
													onClick={addUserToChatHandler}
												>
													Добавить
												</Button>
											</Box>
										</Box>
									}
								</Box>
								{error?.response?.data &&
									<Typography sx={{
										color: '#A05050',
										marginTop: '8px',
									}}>
										{
											typeof error.response.data === 'object' &&
											'message' in error.response.data &&
											error.response?.data?.message as string
										}
									</Typography>
								}
							</>
						</Box>
						<button
							className="leave-button"
							onClick={leaveChatHandler}
						>
							<Typography>
								Выйти из чата
							</Typography>
						</button>
					</Box> :
					<Box sx={{
						backgroundColor: '#CFCFCF',
						width: '400px',
						height: '500px',
						padding: '16px',
						boxSizing: 'border-box',
						display: 'flex',
						alignItems: 'center',
						flexDirection: 'column',
						justifyContent: 'space-between',
						overflow: 'auto',
					}}>
						<Box sx={{
							width: '100%',
							marginTop: '40px',
						}}>
							<AllTags
								headerColor="black"
								header="Выбери теги для чата"
								chosenTags={tagsData?.data.tag_chat_bindings ?? []}
								onUnbindTag={onUnbindTag}
								onBindTag={onBindTag}
							/>
						</Box>
					</Box>
				}
				<Box sx={{
					backgroundColor: '#424242',
					width: '400px',
					height: '500px',
					padding: '16px 0 16px 16px',
					boxSizing: 'border-box',
					overflow: 'auto',
				}}>
					{!isInterestsOpen ?
						<>
							<Box sx={{
								display: 'flex',
								justifyContent: 'space-between',
							}}>
								<Typography sx={{
									fontSize: { xs: '1rem', md: '2rem' },
									mb: 2,
									letterSpacing: 1,
								}}>
									Участники:
								</Typography>
								{
									canChangeChat(chatInfo?.data.role) &&
									chatInfo?.data.type !== 'Personal' &&
									<Button
										roundBorders="left"
										onClick={() => setIsInterestsOpen(true)}
									>
										Интересы
									</Button>
								}
							</Box>
							<ul style={{
								listStyle: 'none',
								padding: '0 16px 0 0',
								margin: 0,
								display: 'flex',
								flexDirection: 'column',
								gap: '8px',
							}}>
								{chatMembers.map((user) => (
									user &&
								<li
									key={user.id}
									style={{
										display: 'flex',
										alignItems: 'center',
										justifyContent: 'space-between',
									}}
								>
									<Box sx={{
										display: 'flex',
										marginLeft: '-16px',
									}}>
										<ChatIcon
											backgroundColor="#CFCFCF"
											color="#000"
											name={user.username.slice(0, 2)}
											onClick={() => openUserProfilePage(user?.id)}
											iconStyle={{
												cursor: 'pointer',
											}}
										/>
										<Typography sx={{
											margin: 'auto 0 auto -8px',
											maxWidth: '128px',
											overflow: 'hidden',
											textOverflow: 'ellipsis',
										}}>
											{user.username}
										</Typography>
									</Box>
									<Box sx={{
										display: 'flex',
									}}>
										{
											(canChangeChat(chatInfo?.data.role) &&
											!canChangeChat(user.membership_type) ||
											isChatOwner(chatInfo?.data.role)) &&
											user.id !== context.state.authUser?.id &&
											<button
												className="block-user"
												style={{
													marginLeft: '8px',
													padding: '8px 16px',
												}}
												onClick={() => onBlockUser(user?.id)}
											>
												<Typography sx={{
													fontSize: '12px',
												}}>
													Заблокировать
												</Typography>
											</button>
										}
										{
											isChatOwner(chatInfo?.data.role) &&
											user.id !== context.state.authUser?.id &&
											!canChangeChat(user.membership_type) &&
											<button
												className="add-moderator"
												style={{
													marginLeft: '8px',
												}}
												onClick={() => onAddModerator(user?.id)}
											>
												<Typography sx={{
													fontSize: '12px',
												}}>
													Сделать<br/>модератором
												</Typography>
											</button>
										}
										{
											!isChatOwner(user.membership_type) &&
											canChangeChat(user.membership_type) &&
											isChatOwner(chatInfo?.data.role) &&
											<button
												className="block-user"
												style={{
													marginLeft: '8px',
												}}
												onClick={() => onDeleteModerator(user?.id)}
											>
												<Typography sx={{
													fontSize: '12px',
												}}>
													Сделать<br/>участником
												</Typography>
											</button>
										}
										{
											isChatOwner(user.membership_type) &&
											<div className="owner-icon">
												<Typography sx={{
													fontSize: '12px',
												}}>
													Owner
												</Typography>
											</div>
										}
									</Box>
								</li>
								))}
							</ul>
						</> :
						<>
							<Box sx={{
								display: 'flex',
								flexDirection: 'row-reverse',
								marginBottom: '8px',
							}}>
								<Button
									roundBorders="left"
									onClick={() => setIsInterestsOpen(false)}
								>
									Назад
								</Button>
							</Box>
							<ChosenTags
								headerColor="black"
								header="Выбранные теги"
								chosenTags={tagsData?.data.tag_chat_bindings ?? []}
								onUnbindTag={onUnbindTag}
							/>
						</>
					}
				</Box>
			</Box>
		</Modal>
	);
};
