import React from 'react';
import { useCookies } from 'react-cookie';
import { useQuery } from '@tanstack/react-query';
import { Navigate, Outlet, useLocation } from 'react-router-dom';
import { getUserFn } from '../../services/api';
import { Actions, useStateContext } from '../../context';
import { Loader } from '../loader/loader';

const RequireUser = ({ allowedRoles }: { allowedRoles: string[] }) => {
	const [cookies] = useCookies(['logged_in']);
	const location = useLocation();
	const stateContext = useStateContext();

	const {
		isLoading,
		isFetching,
		data: user,
	} = useQuery(['authUser'], getUserFn, {
		retry: 1,
		select: (data) => data.data,
		onSuccess: (data) => {
			stateContext.dispatch({ type: Actions.SET_USER, payload: data });
		},
	});

	const loading = isLoading || isFetching;

	if (loading) {
		return <Loader />;
	}

	return user ?
		<Outlet /> :
		<Navigate
			to='/login'
			state={{ from: location }}
			replace
		/>;

	/*
	return (cookies.logged_in || user) &&
    allowedRoles.includes(user?.role as string) ? (
			<Outlet />
		) : cookies.logged_in && user ? (
			<Navigate to='/unauthorized'
				state={{ from: location }}
				replace />
		) : (
			<Navigate to='/login'
				state={{ from: location }}
				replace />
		);
	 */
};

export default RequireUser;
