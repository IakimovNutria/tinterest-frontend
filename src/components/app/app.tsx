import { useRoutes } from 'react-router-dom';
import React from 'react';
import routes from '../../router';
import { Loader } from '../loader/loader';

function App(): JSX.Element {
	return useRoutes(routes) ?? <Loader />;
}

export default App;