import React, { ButtonHTMLAttributes } from 'react';

import './button.css';

type ButtonProps = {
    roundBorders: 'right' | 'left' | 'all' | 'none';
    children: string;
} & ButtonHTMLAttributes<HTMLButtonElement>;

const radius = '16px';

const getBorderRadius = (roundBorders: 'right' | 'left' | 'all' | 'none') => {
	if (roundBorders === 'right') {
		return `0 ${radius} ${radius} 0`;
	}

	if (roundBorders === 'left') {
		return `${radius} 0 0 ${radius}`;
	}

	if (roundBorders === 'all') {
		return `${radius} ${radius} ${radius} ${radius}`;
	}

	if (roundBorders === 'none') {
		return 0;
	}

	return '';
};

function Button(props: ButtonProps): JSX.Element {
	const { children, roundBorders, ...restProps } = props;
	return (
		<button
			className="button"
			style={{
				borderRadius: getBorderRadius(roundBorders),
				padding: '8px 16px',
				height: '1em',
				boxSizing: 'content-box',
			}}
			{...restProps}
		>
			{children}
		</button>
	);
}

export default Button;
