import React from 'react';
import { InputProps } from './input.types';

export const Input = (props: InputProps) => {
	return (
		<input
			style={{
				borderRadius: '25px',
				border: '1px solid #FFDD2D',
				width: '160px',
				height: '36px',
				lineHeight: '36px',
				padding: '0 20px',
				fontSize: '24px',
			}}
			{...props}
		/>
	);
};
