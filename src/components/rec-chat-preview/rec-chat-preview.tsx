import React, { useCallback, useMemo } from 'react';
import { useQuery } from '@tanstack/react-query';
import { Box, Typography } from '@mui/material';
import { IRecommendedChat } from '../../services/api.types';
import { getChatPreviews, joinChat as apiJoinChat } from '../../services/api';
import Button from '../button/button';

type Props = IRecommendedChat;

export const RecChatPreview: React.FC<Props> = (props: Props) => {
	const { data: joinedChats, refetch: refetchJoinedChats } = useQuery(['chatPreviews'], getChatPreviews);
	const joinChat = useCallback(async () => {
		await apiJoinChat(props.id);
		refetchJoinedChats();
	}, [props.id, refetchJoinedChats]);
	const isChatJoined = useMemo(() => {
		return joinedChats?.data.chat_previews.some((chatPreview) => chatPreview.id === props.id);
	}, [joinedChats?.data.chat_previews, props.id]);

	return (
		<Box sx={{
			width: '100%',
			height: '40px',
			backgroundColor: '#CFCFCF',
			borderRadius: '8px',
			display: 'flex',
			paddingLeft: '8px',
			justifyContent: 'space-between',
			alignItems: 'center',
			overflow: 'hidden',
		}}>
			<Typography>
				{props.name}
			</Typography>
			{
				!isChatJoined ?
					<Button
						roundBorders="left"
						onClick={joinChat}
					>
						Вступить
					</Button>
					:
					<Typography sx={{
						borderRadius: '16px 0 0 16px',
						padding: '8px 16px',
						height: '1em',
						boxSizing: 'content-box',
						backgroundColor: '#FF4500',
						display: 'flex',
						alignItems: 'center',
						justifyContent: 'center',
					}}
					>
						<span>
							Вы уже в чате!
						</span>
					</Typography>
			}
		</Box>
	);
};