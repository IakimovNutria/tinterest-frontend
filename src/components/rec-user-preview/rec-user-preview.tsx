import React, { useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import { Box, Typography } from '@mui/material';
import { IUser } from '../../services/api.types';
import Button from '../button/button';
import { createPersonalChat } from '../../services/api';

export const RecUserPreview = (props: IUser & { onCreatePersonalChat: () => void }) => {
	const navigate = useNavigate();
	const redirectOnUserPage = () => {
		navigate(`/user/${props.id}`);
	};
	const createChat = useCallback(() => {
		createPersonalChat(props.id).then(() => props.onCreatePersonalChat());
	}, [props.id]);

	return (
		<Box sx={{
			width: '100%',
			height: '40px',
			backgroundColor: '#CFCFCF',
			borderRadius: '8px',
			display: 'flex',
			paddingLeft: '8px',
			justifyContent: 'space-between',
			alignItems: 'center',
			overflow: 'hidden',
		}}>
			<Typography>
				{props.username}
			</Typography>
			{
				<Box sx={{
					display: 'flex',
				}}>
					<Button
						roundBorders="left"
						onClick={redirectOnUserPage}
					>
						Профиль
					</Button>
					<Button
						roundBorders="none"
						onClick={createChat}
					>
						Создать чат
					</Button>
				</Box>
			}
		</Box>
	);
};
