import React, { CSSProperties } from 'react';
import { Grid, Typography } from '@mui/material';

type Props = {
    backgroundColor: string;
    color: string;
    name: string;
	iconStyle?: CSSProperties;
	onClick?: () => void;
};

export const ChatIcon: React.FC<Props> = (props: Props) => {
	const { backgroundColor, color, name, iconStyle, onClick } = props;

	return (
		<Grid
			item
			sx={{
				margin: '0 16px',
				width: '48px',
			}}
		>
			<Typography
				sx={{
					borderRadius: '48px',
					backgroundColor: backgroundColor,
					width: '48px',
					height: '48px',
					textAlign: 'center',
					lineHeight: '48px',
					marginBlockEnd: '0',
					marginBlockStart: '0',
					color: color,
					...iconStyle,
				}}
				onClick={onClick}
			>
				{name.slice(0, 2)}
			</Typography>
		</Grid>
	);
};