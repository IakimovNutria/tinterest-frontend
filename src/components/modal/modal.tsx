import React, { useCallback, MouseEvent } from 'react';
import { createPortal } from 'react-dom';
import { Box } from '@mui/material';
import { ModalProps } from './modal.types';

export const Modal = (props: ModalProps): JSX.Element => {
	const { onClose, isActive, children } = props;

	const handleClick = useCallback(() => {
		onClose();
	}, [onClose]);

	const stopPropagation = (e: MouseEvent<HTMLDivElement>) => {
		e.stopPropagation();
	};

	return isActive ? createPortal(
		<Box
			sx={{
				width: '100vw',
				height: '100vh',
				background: 'rgba(0, 0, 0, 0.3)',
				position: 'absolute',
				display: 'flex',
				alignItems: 'center',
				justifyContent: 'center',
				top: 0,
				left: 0,
			}}
			onClick={handleClick}
		>
			<Box
				sx={{
					borderRadius: '16px',
					overflow: 'hidden',
				}}
				onClick={stopPropagation}
			>
				{children}
			</Box>
		</Box>,
		document.getElementById('root') ?? document.body
	) : <></>;
};