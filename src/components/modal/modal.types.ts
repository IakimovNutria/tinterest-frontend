export interface ModalProps {
    onClose: () => void;
    isActive: boolean;
    children?: JSX.Element;
}
