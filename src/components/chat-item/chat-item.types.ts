import { ChatPreview } from '../../services/api.types';

export type ChatItemProps = {
    isActive?: boolean;
    onClick?: () => void;
    chatPreview: ChatPreview;
}
