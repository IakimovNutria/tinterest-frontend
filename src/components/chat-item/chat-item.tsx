import React, { useMemo } from 'react';
import { ChatItemProps } from './chat-item.types';
import { Grid, Typography } from '@mui/material';
import DoneALlIcon from '@mui/icons-material/DoneAll';
import CheckIcon from '@mui/icons-material/Check';
import { getActiveFirstColor, getActiveSecondColor } from '../../helpers/chat-helpers';
import { getHumanReadableTime } from '../../helpers/time-converters';
import { useStateContext } from '../../context';
import { ChatIcon } from '../chat-icon/chat-icon';


function ChatItem(props: ChatItemProps): JSX.Element {
	const context = useStateContext();
	const { isActive, onClick, chatPreview: { unread_message_count, name, last_message, id } } = props;
	const containerActiveStyle = isActive ? {
		background: '#CFCFCF',
	} : {};
	const activeSecondColor = getActiveSecondColor(Boolean(isActive));
	const activeFirstColor = getActiveFirstColor(Boolean(isActive));
	const lastMessageTime = useMemo(() => {
		return getHumanReadableTime(last_message?.created_at);
	}, [last_message?.created_at]);

	return (
		<Grid
			container
			sx={{
				height: '80px',
				borderWidth: '0 0 1px 0',
				borderStyle: 'solid',
				borderColor: '#CFCFCF',
				cursor: 'pointer',
				...containerActiveStyle,
			}}
			justifyContent="row"
			alignItems="center"
			onClick={onClick}
		>
			<ChatIcon
				backgroundColor={activeSecondColor}
				color={activeFirstColor}
				name={name}
			/>
			<Grid
				item
				sx={{
					width: '128px',
					marginRight: '16px',
				}}
			>
				<Typography
					sx={{
						fontFamily: 'Roboto',
						fontSize: '16px',
						fontStyle: 'normal',
						fontWeight: 600,
						lineHeight: 'normal',
						color: activeSecondColor,
						margin: '0px 0px 8px 0px',
						overflow: 'hidden',
						textOverflow: 'ellipsis',
						whiteSpace: 'nowrap',
					}}
				>
					{name}
				</Typography>
				<Typography
					sx={{
						color: activeSecondColor,
						fontFamily: 'Roboto',
						fontSize: '16px',
						fontStyle: 'normal',
						fontWeight: 200,
						lineHeight: 'normal',
						overflow: 'hidden',
						textOverflow: 'ellipsis',
						whiteSpace: 'nowrap',
					}}
				>
					{last_message?.text}
				</Typography>
			</Grid>
			<Grid
				item
				sx={{
					width: '64px',
				}}
			>
				<Grid
					container
					justifyContent="center"
					alignItems="center"
					flexDirection="column"
					gap="8px"
				>
					<Typography
						sx={{
							color: activeSecondColor,
							fontFamily: 'Roboto',
							fontSize: '15px',
							fontStyle: 'normal',
							fontWeight: 100,
							lineHeight: 'normal',
						}}
					>
						{lastMessageTime}
					</Typography>
					{
						props.chatPreview.last_message?.user_id !== context.state.authUser?.id ?
							(
								unread_message_count ?
									<Typography
										sx={{
											width: '16px',
											height: '16px',
											borderRadius: '16px',
											backgroundColor: '#FFDD2D',
											color: '#424242',
											fontFamily: 'Roboto',
											fontSize: '15px',
											fontStyle: 'normal',
											fontWeight: 500,
											lineHeight: '16px',
											display: 'flex',
											justifyContent: 'center',
											alignItems: 'center',
										}}
									>
										{unread_message_count}
									</Typography>
									:
									<>
									</>
							)
							:
							(
								props.chatPreview.last_message?.is_read ?
									<DoneALlIcon
										sx={{
											color: '#9F9F9F',
										}}
									/>
									:
									<CheckIcon
										sx={{
											color: '#9F9F9F',
										}}
									/>
							)
					}
				</Grid>
			</Grid>
		</Grid>
	);
}

export default ChatItem;
