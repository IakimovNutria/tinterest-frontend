export type MessageProps = {
    id: number,
    text: string,
    createdAt: string,
    userId: number,
    isRead: boolean
}
