import React, { useEffect, useState } from 'react';
import CheckIcon from '@mui/icons-material/Check';
import DoneALlIcon from '@mui/icons-material/DoneAll';
import { useNavigate } from 'react-router-dom';
import { MessageProps } from './message.types';
import { Box, Typography } from '@mui/material';
import { useStateContext } from '../../context';
import { IUser } from '../../services/api.types';
import { getUserById } from '../../services/api';
import { getHumanReadableTime } from '../../helpers/time-converters';


export function Message(props: MessageProps): JSX.Element {
	const navigate = useNavigate();
	const context = useStateContext();
	const [user, setUser] = useState<IUser>();
	useEffect(() =>
		void(getUserById(props.userId).then((res) => setUser(res.data))),
	[]);

	return (
		<Box
			sx={{
				minHeight: '2em',
				width: '100%',
				color: 'black',
				display: 'flex',
				flexDirection: props.userId === context.state.authUser?.id ? 'row-reverse' : 'row',
			}}
		>
			<Box sx={{
				width: '30%',
				borderRadius: '16px',
				boxSizing: 'border-box',
				border: '2px solid #CFCFCF',
				backgroundColor: props.userId === context.state.authUser?.id ? 'rgba(50, 50, 100)' : '#424242',
				color: '#CFCFCF',
				padding: '16px',
			}}>
				<Typography sx={{
					fontSize: '16px',
				}}>
					{props.text}
				</Typography>
				<Box sx={{
					display: 'flex',
					flexDirection: 'row',
					justifyContent: 'space-between',
				}}>
					{
						props.userId === context.state.authUser?.id &&
						(
							props.isRead ?
								<DoneALlIcon
									sx={{
										color: '#9F9F9F',
									}}
								/>
								:
								<CheckIcon
									sx={{
										color: '#9F9F9F',
									}}
								/>
						)

					}
					{
						props.userId !== context.state.authUser?.id &&
						<Typography
							sx={{
								fontSize: '15px',
								color: '#9F9F9F',
								cursor: 'pointer',
								':hover': {
									textDecoration: 'underline',
								},
							}}
							onClick={
								()=>{navigate(`/user/${user?.id}`);}
							}
						>
							{user?.username}
						</Typography>
					}
					<Typography sx={{
						fontSize: '15px',
						color: '#9F9F9F',
					}}>
						{getHumanReadableTime(props.createdAt)}
					</Typography>
				</Box>
			</Box>
		</Box>
	);
}