import React, { useState, useCallback } from 'react';
import { Box, Typography } from '@mui/material';
import { Modal } from '../modal/modal';
import { AddChatModalProps } from './add-chat-modal.types';
import { IUser, IChatChosenTag, IChosenTag } from '../../services/api.types';
import Button from '../button/button';
import { Input } from '../input/input';
import { bindTagToChat, createGroupChat, getUserByUsername } from '../../services/api';
import { useStateContext } from '../../context';
import { AllTags, ChosenTags } from '../tags-chose/tags-chose';
import { ChatIcon } from '../chat-icon/chat-icon';

export const AddChatModal = (props: AddChatModalProps): JSX.Element => {
	const { onClose, isActive } = props;
	const context = useStateContext();
	const [newChatMembers, setNewChatMembers] = useState<IUser[]>([]);
	const [chatName, setChatName] = useState('');
	const [newUser, setNewUser] = useState('');
	const [isInterestsOpen, setIsInterestsOpen] = useState(false);
	const [isError, setIsError] = useState(false);
	const [chosenTags, setChosenTags] = useState<IChatChosenTag[]>([]);
	const onUnbindTag = useCallback((tagId: number) => {
		setChosenTags((tags) => tags.filter((tag) => tag.tag_id !== tagId));
	}, [setChosenTags]);
	const onBindTag = useCallback((tag: IChosenTag) => {
		setChosenTags((tags) => tags.concat(tag as IChatChosenTag));
	}, [setChosenTags]);
	const changeIsInterestsOpen = useCallback(() => {
		setIsInterestsOpen((prev) => !prev);
	}, [setIsInterestsOpen]);
	const addUser = useCallback(async () => {
		await getUserByUsername(newUser)
			.then((res) => {
				setIsError(false);
				if (res.status === 200) {
					setNewChatMembers((oldMembers) => {
						if (oldMembers.some((oldMember) => oldMember.id === res.data.id)) {
							return oldMembers;
						}

						return oldMembers.concat(res.data);
					});
				}
			})
			.catch(() => setIsError(true));
		setNewUser('');
	}, [newUser, getUserByUsername, setNewUser, setNewChatMembers]);
	const createChat = useCallback(async () => {
		createGroupChat(chatName, newChatMembers.map((user) => user.id).concat(context.state.authUser?.id ?? []))
			.then(async (res) => {
				for (let i = 0; i < chosenTags.length; i++) {
					await bindTagToChat(res.data.id, chosenTags[i].tag_id);
				}
				setChosenTags([]);
				setNewChatMembers([]);
				setChatName('');
				setNewUser('');
				onClose();
			});
	}, [createGroupChat, chatName, newChatMembers, setChatName,
		setNewChatMembers, setNewUser, onClose, chosenTags, bindTagToChat, setChosenTags]);

	return (
		<Modal
			onClose={onClose}
			isActive={isActive}
		>
			<Box sx={{
				width: '800px',
				height: '500px',
				display: 'flex',
			}}>
				<Box sx={{
					height: '100%',
					backgroundColor: '#D9D9D9',
					borderRadius: '16px 0 0 16px',
					overflow: 'hidden',
					width: '50%',
					overflowX: 'auto',
					overflowY: 'auto',
				}}>
					{!isInterestsOpen ?
						<Box sx={{
							backgroundColor: '#D9D9D9',
							padding: '40px',
						}}>
							<Box sx={{
								marginBottom: '16px',
							}}>
								<Typography sx={{
									marginBottom: '8px',
									fontSize: { xs: '0.5rem', md: '1rem' },
									mb: 2,
									letterSpacing: 0.5,
								}}>
									Название чата:
								</Typography>
								<Input
									placeholder="chat name"
									onChange={(e) => setChatName(e.target.value)}
									value={chatName}
								/>
							</Box>
							<Box sx={{
								display: 'flex',
							}}>
								<Box sx={{
									marginRight: '8px',
								}}>
									<Typography sx={{
										fontSize: { xs: '0.5rem', md: '1rem' },
										mb: 2,
										letterSpacing: 0.5,
										marginBottom: '8px',
									}}>
										Добавить участника
									</Typography>
									<Input
										value={newUser}
										onChange={(e) => setNewUser(e.target.value)}
										placeholder="username"
									/>
								</Box>
								<Box sx={{
									display: 'flex',
									flexDirection: 'column-reverse',
								}}>
									<Button
										roundBorders="all"
										onClick={addUser}
									>
										Добавить
									</Button>
								</Box>
							</Box>
							<Typography sx={{
								color: '#A05050',
								textAlign: 'center',
								marginTop: '8px',
							}}>
								{
									isError &&
									'User not found.'
								}
							</Typography>
						</Box> :
						<Box sx={{
							width: '100%',
							marginTop: '40px',
						}}>
							<AllTags
								header="Выбери теги для чата"
								headerColor="black"
								chosenTags={chosenTags}
								onUnbindTag={onUnbindTag}
								onBindTag={onBindTag}
							/>
						</Box>
					}
				</Box>
				<Box sx={{
					height: '100%',
					width: '400px',
					display: 'flex',
					backgroundColor: '#424242',
					borderRadius: '0 16px 16px 0',
					justifyContent: 'space-between',
					flexDirection: 'column',
					alignItems: 'center',
					overflow: 'auto',
				}}>
					{!isInterestsOpen ?
						<>
							<Box sx={{
								margin: '8px 0 16px 16px',
								width: 'calc(100% - 16px)',
							}}>
								<Box sx={{
									display: 'flex',
									justifyContent: 'space-between',
									width: '100%',
								}}>
									<Typography sx={{
										fontSize: { xs: '1rem', md: '2rem' },
										mb: 2,
										letterSpacing: 1,
									}}>
										Участники:
									</Typography>
									<Button
										roundBorders="left"
										onClick={changeIsInterestsOpen}
									>
										Интересы
									</Button>
								</Box>
								<ul style={{
									listStyle: 'none',
									padding: 0,
									overflow: 'auto',
									display: 'flex',
									flexDirection: 'column',
									gap: '8px',
								}}>
									{newChatMembers.map((user) => (
										<li
											key={user.id}
											style={{
												display: 'flex',
												alignItems: 'center',
												gap: '8px',
											}}
										>
											<ChatIcon
												backgroundColor="#CFCFCF"
												color="#000"
												name={user.username.slice(0, 2)}
											/>
											{user.username}
										</li>
									))}
								</ul>
							</Box>
							<Box sx={{
								display: 'flex',
								flexDirection: 'row-reverse',
								width: '100%',
								marginBottom: '16px',
							}}>
								<Button
									roundBorders="left"
									onClick={createChat}
								>
									Создать
								</Button>
							</Box>
						</> :
						<Box sx={{
							width: '100%',
							overflow: 'auto',
							height: '100%',
						}}>
							<Box sx={{
								display: 'flex',
								flexDirection: 'row-reverse',
								width: '100%',
								paddingTop: '8px',
							}}>
								<Button
									roundBorders="left"
									onClick={changeIsInterestsOpen}
								>
									Назад
								</Button>
							</Box>
							<ChosenTags
								headerColor="black"
								chosenTags={chosenTags}
								onUnbindTag={onUnbindTag}
								header="Выбранные теги"
							/>
						</Box>
					}
				</Box>
			</Box>
		</Modal>
	);
};

