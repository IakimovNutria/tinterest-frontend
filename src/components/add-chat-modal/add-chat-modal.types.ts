import { ModalProps } from '../modal/modal.types';

export type AddChatModalProps = Omit<ModalProps, 'children'>;
