import React, { Suspense, lazy } from 'react';
import type { RouteObject } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Loader } from '../components/loader/loader';
import MyProfilePage from '../pages/my-profile-page/my-profile-page';
import RequireUser from '../components/require-user/require-user';
import Layout from '../components/layout/layout';
import { Admin } from '../pages/admin/admin';
import UserProfilePage from '../pages/user-profile-page/user-profile-page';

const Loadable = (Component: React.ComponentType) => function LoadableComponent(props: JSX.IntrinsicAttributes) {
	return (
		<Suspense fallback={<Loader/>}>
			<Component {...props} />
		</Suspense>
	);
};

const LoginPage = Loadable(lazy(() => import('../pages/login/login')));
const RegisterPage = Loadable(lazy(() => import('../pages/registration/registration')));
const HomePage = Loadable(lazy(() => import('../pages/home/home')));

const authRoutes: RouteObject = {
	path: '*',
	children: [
		{
			path: 'login',
			element: <LoginPage />,
		},
		{
			path: 'register',
			element: <RegisterPage />,
		},
	],
};

const normalRoutes: RouteObject = {
	path: '*',
	element: <Layout />,
	children: [
		{
			index: true,
			element: <HomePage />,
		},
		{
			path: 'home/:id',
			element: <HomePage />,
		},
		{
			path: 'profile',
			element: <RequireUser allowedRoles={['user', 'admin']} />,
			children: [
				{
					path: '',
					element: <MyProfilePage />,
				},
			],
		},
		{
			path: 'admin',
			element: <Admin />,
		},
		{
			path: 'user/:id',
			element: <UserProfilePage />,
		},
	],
};

const routes: RouteObject[] = [authRoutes, normalRoutes];

export default routes;
