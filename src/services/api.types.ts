export interface IUser {
    username: string;
    email: string;
    id: number;
    telegram_username: string;
}

export interface GenericResponse {
    status: string;
    message: string;
}

export interface ILoginResponse {
    status: string;
    access_token: string;
}

export interface IMessage {
    id: number;
    text: string;
    created_at: string;
    user_id: number;
    is_read: boolean;
}

export interface ITag {
    id: number;
    name: string;
    description: string;
    parent_tag_id: number;
    approved: boolean;
}

export interface ITags {
    tags: ITag[];
}

export interface ChatPreview {
    id: number;
    name: string;
    unread_message_count: 0;
    last_message: IMessage | null;
}

export interface IChosenTag {
    id: number,
    tag_id: number,
    tag_name: string,
    rating: number,
}

export interface IUserChosenTag extends IChosenTag {
    user_id: number,
}

export interface IChatChosenTag extends IChosenTag {
    chat_id: number,
}

export interface IUserTags {
    tag_user_bindings: IUserChosenTag[];
}

export interface IChatTags {
    tag_chat_bindings: IChatChosenTag[];
}

export interface IRecommendedChat {
    id: number,
    name: string,
    type: string,
    members_count: number,
}

export interface IRecommendedChats {
    chats: IRecommendedChat[];
}

export interface IRecommendedUsers {
    users: IUser[];
}

export interface IChatMembers {
    chat_members: {
        user_id: number,
        membership_type: string,
    }[];
}

export interface ICreatedChat {
    id: number,
    name: string,
    type: string,
    members_count: number,
}

export interface IChatInfo extends ICreatedChat {
    role: string;
}
