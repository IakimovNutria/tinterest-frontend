export function getChatMessagesApiRoute(chatId: number) {
	return `/chat/${chatId}/messages`;
}

export function sendMessageApiRoute(chatId: number) {
	return `/chat/${chatId}/send`;
}

export function getUserInfoApiRoute(userId: number) {
	return `/user/${userId}/info`;
}

export function getJoinChatApiRoute(chatId: number) {
	return `/chat/${chatId}/join`;
}

export function getLeaveChatApiRoute(chatId: number) {
	return `/chat/${chatId}/leave`;
}

export function getInviteChatApiRoute(chatId: number) {
	return `/chat/${chatId}/invite`;
}

export function getRemoveChatUserApiRoute(chatId: number) {
	return `/chat/${chatId}/block`;
}

export function getUpdateAccessChatUserApiRoute(chatId: number) {
	return `/chat/${chatId}/access`;
}

export function getUpdateLastReadMessageApiRoute(chatId: number) {
	return `/chat/${chatId}/read`;
}

export function getChatInfoApiRoute(chatId: number) {
	return `/chat/${chatId}/info`;
}

export function getChatMembersApiRoute(chatId: number) {
	return `/chat/${chatId}/users`;
}

export function getTagInfoApiRoute(tagId: number) {
	return `/tag/${tagId}/info`;
}

export function getApproveTagApiRoute(tagId: number) {
	return `/tag/${tagId}/approve`;
}

export function getDeleteTagApiRoute(tagId: number) {
	return `/tag/${tagId}/delete`;
}

export function getUpdateTagNameApiRoute(tagId: number) {
	return `/tag/${tagId}/update/name`;
}

export function getUpdateTagDescriptionApiRoute(tagId: number) {
	return `/tag/${tagId}/update/description`;
}

export function getUpdateParentTagApiRoute(tagId: number) {
	return `/tag/${tagId}/update/parent_tag`;
}

export function getUserTagsApiRoute(userId: number) {
	return `/tag_user_binding/${userId}/get`;
}

export function getChatTagsApiRoute(chatId: number) {
	return `/tag_chat_binding/${chatId}/get`;
}

export function getUserByUsernameApiRoute(username: string) {
	return `/user/${username}/resolve`;
}