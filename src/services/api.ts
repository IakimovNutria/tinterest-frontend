import axios, { AxiosResponse } from 'axios';
import { LoginInput } from '../constants/login';
import { RegisterInput } from '../constants/register';
import {
	ChatPreview,
	GenericResponse, IChatInfo, IChatMembers, IChatTags,
	ICreatedChat,
	ILoginResponse,
	IMessage,
	IRecommendedChats,
	IRecommendedUsers,
	ITags,
	IUser,
	IUserTags
} from './api.types';
import { ApiRoute, BACKEND_URL, FRONT_SERVER_PATH, REQUEST_TIMEOUT } from './api.constants';
import {
	getChatMessagesApiRoute,
	getUserByUsernameApiRoute,
	getUserInfoApiRoute,
	getUserTagsApiRoute,
	sendMessageApiRoute,
	getUpdateLastReadMessageApiRoute,
	getJoinChatApiRoute,
	getChatMembersApiRoute,
	getInviteChatApiRoute,
	getLeaveChatApiRoute,
	getChatTagsApiRoute,
	getChatInfoApiRoute,
	getApproveTagApiRoute,
	getDeleteTagApiRoute,
	getRemoveChatUserApiRoute,
	getUpdateAccessChatUserApiRoute
} from './api.helpers';
import { ChatType } from '../constants/chat';
import { StatusCodes } from 'http-status-codes';
import { toast } from 'react-toastify';

export const api = axios.create({
	baseURL: BACKEND_URL,
	timeout: REQUEST_TIMEOUT,
	headers: {
		'Access-Control-Allow-Origin': FRONT_SERVER_PATH,
		'Access-Control-Allow-Credentials': true,
	},
	withCredentials: true,
});

api.defaults.headers.common['Content-Type'] = 'application/json';

export const refreshAccessTokenFn = async () => {
	return await api.get<ILoginResponse>('auth/refresh');
};

const StatusCodeMapping: number[] = [
	StatusCodes.BAD_REQUEST,
	StatusCodes.UNAUTHORIZED,
	StatusCodes.NOT_FOUND,
];

const shouldDisplayError = (response: AxiosResponse): boolean =>
	StatusCodeMapping.includes(response.status);

enum Errors {
    VALIDATION_ERROR = 'USER_NOT_FOUND',
    COMMON_ERROR = 'COMMON_ERROR',
}

api.interceptors.response.use(
	(response) => {
		return response;
	},
	async (error) => {
		const originalRequest = error.config;
		const errMessage = error?.response?.data?.message as string ?? '';
		if (errMessage.includes('not logged in') && !originalRequest._retry) {
			originalRequest._retry = true;
			await refreshAccessTokenFn();
			return api(originalRequest);
		}
		if (error?.response?.data?.message?.includes('not refresh')) {
			document.location.href = '/login';
		}
		if (error.response && shouldDisplayError(error.response)) {
			const { error_id, message } = error.response.data;
			toast.error(message);
		}
		return Promise.reject(error);
	}
);

export const loginUserFn = async (user: LoginInput) => {
	return await api.post<ILoginResponse>(ApiRoute.USER_AUTHENTICATION, user);
};

export const signUpUserFn = async (user: RegisterInput) => {
	const response = await api.post<GenericResponse>(ApiRoute.USER_REGISTRATION, user);
	if (response.status === 201) {
		return await loginUserFn(user);
	}
};

export const getUserFn = async () => {
	return await api.get<IUser>(ApiRoute.ME);
};

export const getChatMessages = (chat_id: number, startMessageId: number, limit=25, direction: 'to_old' | 'to_new' = 'to_old') => async () => {
	if (startMessageId === -1) {
		return { data: { messages: [] } };
	}
	return await api.get<{ messages: IMessage[] }>(getChatMessagesApiRoute(chat_id), { params: { start_message_id: startMessageId, limit, direction } });
};

export const sendMessage = async (chat_id: number, text: string) => {
	return await api.post<GenericResponse>(sendMessageApiRoute(chat_id), { text });
};

export const getChatPreviews = async () => {
	return await api.get<{ chat_previews: ChatPreview[] }>(ApiRoute.CHAT_PREVIEWS);
};

export const markMessageRead = async (chat_id: number, messageId: number) => {
	return await api.patch<GenericResponse>(getUpdateLastReadMessageApiRoute(chat_id), { message_id: messageId });
};

export const getTags = async (tag_id?: number) => {
	return await api.get<ITags>(ApiRoute.CHILDREN_TAGS, { params: tag_id ? { tag_id } : { } });
};

export const getUserByUsername = async (username: string) => {
	return await api.get<IUser>(getUserByUsernameApiRoute(username));
};

export const createGroupChat = async (name: string, users_ids: number[]) => {
	return await api.post<ICreatedChat>(ApiRoute.CREATE_CHAT, { type: ChatType.PUBLIC, name, users_ids });
};
export const createTag = async (name: string, description: string, parent_tag_id?: number) => {
	return await api.post<GenericResponse>(ApiRoute.CREATE_TAG, {
		name: name,
		description: description,
		parent_tag_id: parent_tag_id,
	});
};

export const getUserById = async (id: number) => {
	return await api.get<IUser>(getUserInfoApiRoute(id));
};

export const getUsersTags = async (user_id: number) => {
	return await api.get<IUserTags>(getUserTagsApiRoute(user_id));
};

export const bindTagToUser = async (tag_id: number, rating: number)=> {
	return await api.post(ApiRoute.BIND_TAG_TO_USER, { tag_id, rating });
};

export const unbindUserTag = async (tag_id: number) => {
	return await api.delete(ApiRoute.UNBIND_TAG_FROM_USER, { data: { tag_id } });
};

export const updateUserTag = async (tag_id: number, rating: number) => {
	return await api.patch(ApiRoute.UPDATE_USER_TAG, { tag_id, rating });
};

export const getRecommendedChats = async () => {
	return await api.get<IRecommendedChats>(ApiRoute.RECOMMENDED_CHATS);
};

export const getRecommendedUsers = async () => {
	return await api.get<IRecommendedUsers>(ApiRoute.RECOMMENDED_USERS);
};

export const bindTagToChat = async (chat_id: number, tag_id: number) => {
	return await api.post<GenericResponse>(ApiRoute.BIND_TAG_TO_CHAT, { chat_id, tag_id });
};

export const joinChat = async (chat_id: number) => {
	return await api.post<GenericResponse>(getJoinChatApiRoute(chat_id));
};

export const getChatMembers = (chat_id: number) => async () => {
	return await api.get<IChatMembers>(getChatMembersApiRoute(chat_id));
};

export const addUserToChat = async (chat_id: number, user_id: number) => {
	return await api.post<GenericResponse>(getInviteChatApiRoute(chat_id), { user_id });
};

export const leaveChat = async (chat_id: number) => {
	return await api.delete<GenericResponse>(getLeaveChatApiRoute(chat_id));
};

export const getChatTags = async (chat_id: number) => {
	return await api.get<IChatTags>(getChatTagsApiRoute(chat_id));
};

export const unbindChatTag = async (chat_id: number, tag_id: number) => {
	return await api.delete<GenericResponse>(ApiRoute.UNBIND_TAG_FROM_CHAT, { data: { tag_id, chat_id } });
};

export const getUnapprovedTags = () => {
	return api.get<ITags>(ApiRoute.UNAPPROVED_TAGS);
};

export const getChatInfo = async (chat_id: number) => {
	return await api.get<IChatInfo>(getChatInfoApiRoute(chat_id));
};

export const approveTag = async (tag_id: number) => {
	return await api.patch(getApproveTagApiRoute(tag_id));
};

export const deleteTag = async (tag_id: number) => {
	return await api.delete(getDeleteTagApiRoute(tag_id));
};

export const blockUser = async (chat_id: number, user_id: number) => {
	return await api.delete(getRemoveChatUserApiRoute(chat_id), { data: { user_id } });
};

export const updateUserAccessRole = async (chat_id: number, user_id: number, membership_type: string) => {
	return await api.patch(getUpdateAccessChatUserApiRoute(chat_id), { user_id, membership_type });
};

export const createPersonalChat = async (user_id: number) => {
	return await api.post(ApiRoute.CREATE_PERSONAL_CHAT, { user_id });
};


