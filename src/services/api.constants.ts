export const FRONT_SERVER_PATH = 'http://51.250.72.142:3000';
export const BACKEND_URL = 'http://51.250.72.142:8054/api/v1';
export const REQUEST_TIMEOUT = 10000;


// export const FRONT_SERVER_PATH = 'http://localhost:3000';
// export const BACKEND_URL = 'http://localhost:8080/api/v1';
// export const REQUEST_TIMEOUT = 10000;

export enum ApiRoute {
    USER_AUTHENTICATION = '/auth',
    USER_REGISTRATION = '/user/create',
    ME = '/user/me',
    LOGOUT = '/logout',
    CHAT_PREVIEWS = '/chat/previews',
    RECOMMENDED_USERS = '/recsys/users',
    RECOMMENDED_CHATS = '/recsys/chats',
    CREATE_CHAT = '/chat/create',
    CREATE_PERSONAL_CHAT = '/chat/create/personal',
    CREATE_TAG = '/tag/create',
    CHILDREN_TAGS = '/tag/children',
    UNITE_TAGS = '/tag/unite',
    UNAPPROVED_TAGS = '/tag/unapproved',
    BIND_TAG_TO_USER = '/tag_user_binding/create',
    UNBIND_TAG_FROM_USER = '/tag_user_binding/delete',
    UPDATE_USER_TAG = '/tag_user_binding/update',
    BIND_TAG_TO_CHAT = '/tag_chat_binding/create',
    UNBIND_TAG_FROM_CHAT = '/tag_chat_binding/delete',
}
