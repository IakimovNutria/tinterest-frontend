import React from 'react';
import { ITag, IUser } from '../services/api.types';

export enum Actions {
	SET_USER = 'SET_USER',
	SET_TAGS = 'SET_TAGS',
	ADD_TAG = 'ADD_TAG',
}

type State = {
    authUser: IUser | null;
	tags: ITag[] | null;
};

type SetUserAction = {
    type: Actions.SET_USER;
    payload: IUser | null;
};

type SetTagsAction = {
	type: Actions.SET_TAGS;
	payload: ITag[] | null;
};

type AddTagAction = {
	type: Actions.ADD_TAG;
	payload: ITag | null;
};

type Action = SetUserAction | SetTagsAction | AddTagAction;

type Dispatch = (action: Action) => void;

const initialState: State = {
	authUser: null,
	tags: [],
};

type StateContextProviderProps = { children: React.ReactNode };

const StateContext = React.createContext<
    { state: State; dispatch: Dispatch } | undefined
>(undefined);

const stateReducer = (state: State, action: Action): State => {
	switch (action.type) {
		case 'SET_USER': {
			return {
				...state,
				authUser: action.payload,
			};
		}
		case 'SET_TAGS': {
			return {
				...state,
				tags: action.payload,
			};
		}
		case 'ADD_TAG': {
			if (!state.tags) {
				return {
					...state,
					tags: action.payload ? [action.payload] : [],
				};
			}
			return {
				...state,
				tags: state.tags.concat(action.payload ?? []),
			};
		}
		default: {
			throw new Error('Unhandled action type');
		}
	}
};

const StateContextProvider = ({ children }: StateContextProviderProps) => {
	const [state, dispatch] = React.useReducer(stateReducer, initialState);
	const value = { state, dispatch };
	return (
		<StateContext.Provider value={value}>{children}</StateContext.Provider>
	);
};

const useStateContext = () => {
	const context = React.useContext(StateContext);

	if (context) {
		return context;
	}

	throw new Error('useStateContext must be used within a StateContextProvider');
};

export { StateContextProvider, useStateContext };
